# Codebase release 0.1 for infstat

by Kyoungchul Kong, Konstantin T. Matchev, Stephen Mrenna, Prasanth Shyamsundar

SciPost Phys. Codebases 14-r0.1 (2023) - published 2023-07-07

[DOI:10.21468/SciPostPhysCodeb.14-r0.1](https://doi.org/10.21468/SciPostPhysCodeb.14-r0.1)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.14-r0.1) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Kyoungchul Kong, Konstantin T. Matchev, Stephen Mrenna, Prasanth Shyamsundar

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://gitlab.com/prasanthcakewalk/code-and-data-availability/-/tree/bfd3ea83712d53f2e1f84d8e040d6d9b5bc71c04/arXiv_2210.01680](https://gitlab.com/prasanthcakewalk/code-and-data-availability/-/tree/bfd3ea83712d53f2e1f84d8e040d6d9b5bc71c04/arXiv_2210.01680)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.14-r0.1](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.14-r0.1)
