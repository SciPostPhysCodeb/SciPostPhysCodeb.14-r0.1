base_seed_dict = {'kse': 10_000, 'klre': 20_000, 'carl': 30_000, 'dctr': 40_000}
def get_traindata_seeds(task, n_reps):
    base = base_seed_dict[task]
    return list(range(base+1, base+1+n_reps))

def get_testdata_seeds(task):
    base = base_seed_dict[task]
    return [base+1_000, base+2_000]

def get_network_seeds(task, network_type, n_reps):
    if network_type == 'inferostatic':
        base = base_seed_dict[task]
    elif network_type == 'direct':
        base = base_seed_dict[task] + 1_000
    
    return list(range(base+1, base+1+n_reps))