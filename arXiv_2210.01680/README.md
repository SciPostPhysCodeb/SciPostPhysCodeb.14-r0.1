This directory contains the code and data relevant to the paper:  
**_New Machine Learning Techniques for Simulation-Based Inference: InferoStatic Nets, Kernel Score Estimation, and Kernel Likelihood Ratio Estimation_**, Kyoungchul Kong, Konstantin T. Matchev, Stephen Mrenna, Prasanth Shyamsundar, [arXiv:2210.01680 [stat.ML]](https://arxiv.org/abs/2210.01680) (2022).  

### Software requirements
The code was tested using `Python 3.10.7`. The following additional packages are required to run the code in this directory. The versions of the packages the code was tested on are given in parantheses.
  * `numpy (1.23.3)`
  * `scipy (1.9.1)`
  * `mpmath (1.2.1)`
  * `matplotlib (3.6.0)`
  * `tensorflow (2.9.2)`

Note: If `tensorflow >= 2.10` is used, the numbers will differ from those in the paper, due to changes in seed handling. The numbers may also differ for `tensorflow < 2.9`.

### Guide to running the code
1. The directory `python_modules/` contains two pure-python modules called `inferostatics` and `kse_utils`. The former contains utilities for creating inferostatic networks in tensorflow, and the latter contains utilities for perform Kernel Score Estimation.
2. The directory `Trivariate Dirichlet Example/` contains the example analysis in [arXiv:2210.01680 [stat.ML]](https://arxiv.org/abs/2210.01680).
3. `Trivariate Dirichlet Example/dirichlet.py` contains definitions related to the Dirichlet distribution.
4. `Trivariate Dirichlet Example/seeds.py` provides the seeds used for data generation and neural network initialization and training.
5. Running `Trivariate Dirichlet Example/data_generation.ipynb` will produce the training and testing datasets for the various tasks and save them in `Trivariate Dirichlet Example/data/`.
6. Running `Trivariate Dirichlet Example/training_{task}.ipynb`, where `{task}` is one of `kse`, `klre` and `carl`, will use the training data in `Trivariate Dirichlet Example/data/` to train the neural networks for the respective task. The trained neural networks will be saved in `Trivariate Dirichlet Example/nn_models/`.
7. After training the networks for _all_ the tasks as above, running `Trivariate Dirichlet Example/evaluation_{task}.ipynb`, where `{task}` is one of `kse`, `klre` and `carl`, will use the testing data in `Trivariate Dirichlet Example/data/` to evaluate the neural networks in `Trivariate Dirichlet Example/nn_models/`. All generated plots will be saved in `Trivariate Dirichlet Example/plots/`.