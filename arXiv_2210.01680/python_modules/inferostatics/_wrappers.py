import tensorflow as tf
from tensorflow import keras
from .layers import ScoreLayer, LLRLayer

class InferostaticsSuite:
    def __init__(self, x_shape, param_shape,
                 potential_model, expand_fn_args=False):
        self.x_shape = x_shape
        self.param_shape = param_shape
        self.potential_model = potential_model
        self.expand_fn_args = expand_fn_args
    
    def make_score_model(self, output_slice=None, output_mask=None):
        x = keras.Input(shape=self.x_shape, name='x')
        param = keras.Input(shape=self.param_shape, name='param')
        score_layer = ScoreLayer(
            potential_fn=self.potential_model,
            expand_fn_args=self.expand_fn_args,
            output_slice=output_slice, param_shape=self.param_shape,
            output_mask=output_mask
        )
        output = score_layer(x, param)
        
        return keras.Model(inputs = [x, param], outputs = output)
    
    def make_llr_model(self, output_mode='categorical_logit',
                       output_for='both'):
        x = keras.Input(shape=self.x_shape, name='x')
        params = [
            keras.Input(shape=self.param_shape, name=f'param_{i}')
            for i in range(2)
        ]
        llr_layer = LLRLayer(
            potential_fn=self.potential_model,
            expand_fn_args=self.expand_fn_args,
            output_mode=output_mode, output_for=output_for
        )
        output = llr_layer(x, params[0], params[1])
        
        return keras.Model(
            inputs = [x, params[0], params[1]],
            outputs = output
        )
    
    def make_score_llr_model(self, score_output_slice=None, 
                             score_output_mask=None, score_output_for='0',
                             score_param_idx_loc='last',
                             llr_output_mode='categorical_logit',
                             llr_output_for='both'):
        # Process score args #########################################
        if score_output_for not in ['0', '1', 'both']:
            raise ValueError(
                "`score_output_for` must be '0', '1', or 'both' (strings)."
            )
        
        if score_param_idx_loc not in ['first', 'last', None]:
            raise ValueError(
                "`score_param_idx_loc` must be 'first', 'last', or None."
            )
        
        if score_output_for == 'both' and score_param_idx_loc is None:
            raise ValueError(
                "`score_param_idx_loc` cannot be None when `score_output_for`='both'."
            )
        
        if score_output_for == '0':
            score_output_for = [0]
        elif score_output_for == '1':
            score_output_for = [1]
        else:
            score_output_for = [0, 1]
        
        if score_param_idx_loc == 'first':
            score_param_idx_loc = 1
        elif score_param_idx_loc == 'last':
            score_param_idx_loc = -1
        ##############################################################
        
        # Process llr arguments ######################################
        llr_output_mode, llr_output_for = LLRLayer._process_output_args(
            output_mode=llr_output_mode, output_for=llr_output_for,
            output_mode_name='llr_output_mode',
            output_for_name='llr_output_for'
        )
        ##############################################################
        
        x = keras.Input(shape=self.x_shape)
        params = [keras.Input(shape=self.param_shape) for _ in range(2)]
        
        categorical_logits = [None, None]
        score_outputs = [None, None]
        
        # Score output #####################################
        score_layer = ScoreLayer(
            potential_fn=self.potential_model,
            expand_fn_args=self.expand_fn_args,
            output_slice=score_output_slice, param_shape=self.param_shape,
            output_mask=score_output_mask,
            return_potential_on_call=True
        )
        
        for i in range(2):
            if i in score_output_for:
                score_outputs[i], categorical_logits[i] = \
                            score_layer(x, params[i])
            else:
                categorical_logits[i] = \
                            score_layer.potential_callable([x, params[1]])
        
        if len(score_output_for) > 1:
            score_output = tf.stack(score_outputs, axis=score_param_idx_loc)
        else:
            score_output = score_outputs[score_output_for[0]]
        ####################################################
        
        # LLR output #######################################
        llr_output = LLRLayer._call_from_categorical_logit(
            categorical_logits,
            output_mode=llr_output_mode, output_for=llr_output_for
        )
        ####################################################
        
        return keras.Model(
            inputs = [x, params[0], params[1]],
            outputs = [score_output, llr_output]
        )
    
    def _make_score_llr_model(self, score_output_slice=None, 
                              score_output_mask=None, score_output_for='0',
                              score_param_idx_loc='first',
                              llr_output_mode='categorical_logit',
                              llr_output_for='both'):
        if score_output_for not in ['0', '1', 'both']:
            raise ValueError(
                "`score_output_for` must be '0', '1', or 'both' (strings)."
            )
        
        if score_param_idx_loc not in ['first', 'last', None]:
            raise ValueError(
                "`score_param_idx_loc` must be 'first', 'last', or None."
            )
        
        if score_output_for == 'both' and score_param_idx_loc is None:
            raise ValueError(
                "`score_param_idx_loc` cannot be None when `score_output_for`='both'."
            )
        
        if score_output_for == '0':
            score_output_for = [0]
        elif score_output_for == '1':
            score_output_for = [1]
        else:
            score_output_for = [0, 1]
        
        if score_param_idx_loc == 'first':
            score_param_idx_loc = 1
        elif score_param_idx_loc == 'last':
            score_param_idx_loc = -1
        
        x = keras.Input(shape=self.x_shape)
        params = [keras.Input(shape=self.param_shape) for i in range(2)]
        
        # Score output #####################################
        score_layer = ScoreLayer(
            potential_fn=self.potential_model,
            expand_fn_args=self.expand_fn_args,
            output_slice=score_output_slice, param_shape=self.param_shape,
            output_mask=score_output_mask
        )
        
        score_outputs = [None, None]
        for i in score_output_for:
            score_outputs[i] = score_layer(x, params[i])
        
        if len(score_output_for) > 1:
            score_output = tf.stack(score_outputs, axis=score_param_idx_loc)
        else:
            score_output = score_outputs[score_output_for[0]]
        ####################################################
        
        # LLR output #######################################
        llr_layer = LLRLayer(
            potential_fn=self.potential_model,
            expand_fn_args=self.expand_fn_args,
            output_mode=llr_output_mode, output_for=llr_output_for
        )
        llr_output = llr_layer(x, params[0], params[1])
        ####################################################
        
        return keras.Model(
            inputs = [x, params[0], params[1]],
            outputs = [score_output, llr_output]
        )