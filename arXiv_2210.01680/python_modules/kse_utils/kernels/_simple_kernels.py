import numpy as np
from ._bounded_kernel_base import _BoundedKernelBase

class Delta(_BoundedKernelBase):
    def _pdf_noscale(self, x):
        return np.where(np.absolute(x)==1, np.nan, 0)
    
    def _sample_noscale(self, size, rng):
        return rng.choice([-1, 1], size=size)
    
    def _absolute_moment_noscale(self, k):
        return 0*k + 1
    
    _allowed_absolute_moments_str = "k must be real valued."
    def _absolute_moment_exists(self, k):
        return True

class Rectangular(_BoundedKernelBase):
    def _pdf_noscale(self, x):
        return np.full_like(x, fill_value=1/2, dtype=float)
    
    def _sample_noscale(self, size, rng):
        return rng.uniform(low=-1, high=+1, size=size)
    
    def _absolute_moment_noscale(self, k):
        return 1/(k+1)
    
    _allowed_absolute_moments_str = "Only k > -1 is allowed."
    def _absolute_moment_exists(self, k):
        return (k > -1)

class Triangular(_BoundedKernelBase):
    def _pdf_noscale(self, x):
        return 1 - np.absolute(x)
    
    def _sample_noscale(self, size, rng):
        return rng.triangular(left=-1, mode=0, right=+1, size=size)
    
    def _absolute_moment_noscale(self, k):
        return 2/(k**2+3*k+2)
    
    _allowed_absolute_moments_str = "Only k > -1 is allowed."
    def _absolute_moment_exists(self, k):
        return (k > -1)