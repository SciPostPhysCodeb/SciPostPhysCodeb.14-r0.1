import numpy as np
from scipy.special import gamma as gamma_func

from ._bounded_kernel_base import _BoundedKernelBase

class TriCube(_BoundedKernelBase):
    pass

class RaisedCosine(_BoundedKernelBase):
    pass

class Trapezoidal(_BoundedKernelBase):
    pass

class TruncatedNormal(_BoundedKernelBase):
    pass

class UQuadratic(_BoundedKernelBase):
    pass

class VonMises(_BoundedKernelBase):
    pass

class ContinuousBernoulli(_BoundedKernelBase):
    pass

class Bates(_BoundedKernelBase):
    def __init__(self, n):
        self.n = n
        super().__init__()
    
    def _sample_noscale(self, size, rng):
        tmp = 0
        for _ in range(self.n):
            tmp += rng.uniform(low=-1, high=+1, size=size)
        return tmp/self.n