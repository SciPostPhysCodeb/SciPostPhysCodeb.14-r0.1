import numpy as np
from scipy.special import gamma as gamma_func
from scipy.special import beta as beta_func
from ._bounded_kernel_base import _BoundedKernelBase

class SymmetricBeta(_BoundedKernelBase):
    def __init__(self, alpha):
        assert alpha > 0
        self.alpha = alpha
        super().__init__()
    
    def _pdf_noscale(self, x):
        nr = (1-x**2)**(self.alpha-1)
        dr = 2**(2*self.alpha - 1) * beta_func(self.alpha, self.alpha)
        return nr/dr
    
    def _sample_noscale(self, size, rng):
        tmp = rng.beta(a=self.alpha, b=self.alpha, size=size)
        return 2*tmp-1
    
    def _absolute_moment_noscale(self, k):
        nr = 2**(1-2*self.alpha) * beta_func(self.alpha, (k+1)/2)
        dr = beta_func(self.alpha, self.alpha)
        return nr/dr
    
    _allowed_absolute_moments_str = "Only k > -1 is allowed."
    def _absolute_moment_exists(self, k):
        return (k > -1)
    
    def _absolute_moment_noscale_alternative(self, k):
        nr = gamma_func(self.alpha+1/2)*gamma_func((k+1)/2)
        dr = np.sqrt(np.pi)*gamma_func(self.alpha+k/2+1/2)
        return nr/dr

class WignerNSphere(SymmetricBeta):
    def __init__(self, n):
        assert n > -1
        super().__init__(alpha=(n+1)/2)

class ArcSine(SymmetricBeta):
    def __init__(self):
        super().__init__(alpha=1/2)

class WignerSemicircle(SymmetricBeta):
    def __init__(self):
        super().__init__(alpha=3/2) # WignerNSphere(n = 2)

class Epanechnikov(SymmetricBeta):
    def __init__(self):
        super().__init__(alpha=2)
    
    def _sample_noscale_alternative(self, size, rng):
        u = [rng.uniform(low=-1, high=+1, size=size) for _ in range(3)]
        
        # sort ui by |ui|, and return either the lowest or second lowest with equal probability
        u_abs = [np.absolute(u[i]) for i in range(3)]
        return np.where(
            np.logical_and(u_abs[2] > u_abs[0], u_abs[2] > u_abs[1]), 
            u[1], u[2]
        )
Parabolic = Epanechnikov

class Quartic(SymmetricBeta):
    def __init__(self):
        super().__init__(alpha=3)
BiWeight = Quartic

class TriWeight(SymmetricBeta):
    def __init__(self):
        super().__init__(alpha=4)